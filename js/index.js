$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 2500
    })
})
$('#contacto').on('show.bs.modal', function (e) {
    console.log('El modal contacto se esta corriendo')
    $('#contactoBtn').removeClass('btn-success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled', true);

})
$('#contacto').on('shown.bs.modal', function (e) {
    console.log('El modal contacto se mostró')
})
$('#contacto').on('hide.bs.modal', function (e) {
    console.log('El modal contacto se oculta')
})
$('#contacto').on('hidden.bs.modal', function (e) {
    console.log('El modal contacto se ocultó')
    $('#contactoBtn').removeClass('btn-primary');
    $('#contactoBtn').addClass('btn-success');
    $('#contactoBtn').prop('disabled', false);
})